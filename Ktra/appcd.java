package Ktra;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Scanner;

public class appcd {
    public static void main(String[] args) {
        ArrayList<TheCD> cdList = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("---------------------------------------");
            System.out.println("--1. Nhập dữ liệu");
            System.out.println("--2. Xuất dữ liệu");
            System.out.println("--3. Hiển thị các bài hát có số bài hát > 5");
            System.out.println("--4. Tính tổng tiền");
            System.out.println("--5. Thoát");
            System.out.println("---------------------------------------");
            System.out.print("Chọn chức năng: ");
            int choice = scanner.nextInt();

            switch (choice) {
                case 1:
                    System.out.print("Số lượng CD muốn thêm: ");
                    int n = scanner.nextInt();
                    for (int i = 0; i < n; i++) {
                        System.out.println("Nhập thông tin CD thứ " + (i+1) + ":");
                        System.out.print("ID: ");
                        int id = scanner.nextInt();
                        scanner.nextLine();
                        System.out.print("Name: ");
                        String name = scanner.nextLine();
                        String date_create = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm"));
                        System.out.print("Price: ");
                        float price = scanner.nextFloat();
                        System.out.print("Number of songs: ");
                        float number = scanner.nextFloat();
                        TheCD cd = new TheCD(id, name, date_create, price, number);
                        cdList.add(cd);
                    }
                    break;
                case 2:
                    for (TheCD cd : cdList) {
                        System.out.println(cd.toString());
                    }
                    break;
                case 3:
                    System.out.println("Các bài hát có số bài hát > 5:");
                    for (TheCD cd : cdList) {
                        if (cd.number > 5) {
                            System.out.println(cd.toString());
                        }
                    }
                    break;
                case 4:
                    float total = 0;
                    for (TheCD cd : cdList) {
                        total += cd.price * cd.number;
                    }
                    System.out.println("Tổng tiền của tất cả các TheCD là: " + total);
                    break;
                case 5:
                    System.out.println("Goodbye!");
                    System.exit(0);
                default:
                    System.out.println("Chức năng không hợp lệ, vui lòng chọn lại!");
            }
        }
    }
}

