package Ktra;
import java.util.Scanner;

public class app {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        if (n % 2 == 0) {
            System.out.println("*");
            for (int i = 2; i <= n / 2; i++) {
                System.out.print("*");
                for (int j = 1; j < i; j++) {
                    System.out.print(" ");
                }
                System.out.println("*");
            }
            for (int i = n / 2; i >= 2; i--) {
                System.out.print("*");
                for (int j = 1; j < i; j++) {
                    System.out.print(" ");
                }
                System.out.println("*");
            }
            System.out.print("*");
        } else {
            System.out.println("*");
            for (int i = 2; i <= (n - 1) / 2; i++) {
                System.out.print("*");

                for (int j = 1; j < i; j++) {
                    System.out.print(" ");
                }
                System.out.println("*");
            }
            System.out.print("*");

            for (int z = 1; z < (n - 1) / 2; z++) {
                System.out.print(" ");
            }
            System.out.println("**");

            for (int i = (n - 1) / 2; i >= 2; i--) {
                System.out.print("*");

                for (int j = 1; j < i; j++) {
                    System.out.print(" ");
                }

                System.out.println("*");
            }

            System.out.print("*");
        }
    }
}
