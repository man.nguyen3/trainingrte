package Ktra;

class TheCD {
    int id;
    String name;
    String date_create;
    float price;
    float number;

    public TheCD(int id, String name, String date_create, float price, float number) {
        this.id = id;
        this.name = name;
        this.date_create = date_create;
        this.price = price;
        this.number = number;
    }

    public String toString() {
        return "ID: " + id + ", Name: " + name + ", Date created: " + date_create +
               ", Price: " + price + ", Number of songs: " + number;
    }
}