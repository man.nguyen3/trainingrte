import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception {
        // bài 2
        String tenHoa = "Hoa mẫu đơn";
        Integer giaBan = 120000;
        Integer soLuong = 4;
        Integer thanhTien = giaBan * soLuong;
        System.out.println("Tên hoa:" + tenHoa);
        System.out.println("Giá bán:" + giaBan + "VNĐ" + "Số lượng" + soLuong);
        System.out.println("Thành tiền:" + thanhTien + "VNĐ");

        // bài 3
        int tongBanGhi = 57;
        int soBanGhiCuaTrang = 10;
        // ép về kiểu int sau khi dùng hàm Math(kiểu mặt định là double)
        int soTrang = (int) Math.ceil((double) tongBanGhi / soBanGhiCuaTrang);
        System.out.println("Số trang làm tròn tăng: " + soTrang);
        soTrang = tongBanGhi / soBanGhiCuaTrang;
        System.out.println("Số trang làm tròn giảm: " + soTrang);

        // bài 4
        // a
        Scanner scanner = new Scanner(System.in);
        System.out.print("Nhập 1 số vào đây: ");
        Integer num = scanner.nextInt();
        if (num % 3 != 0) {
            System.out.println("Số này không chia hết cho 3");
        } else {
            System.out.println("Số này chia hết cho 3");
        }
        // b
        if (num >= 15 || num <= 90) {
            System.out.println("“Số này nằm trong khoảng từ 15 đến 90");
        } else {
            System.out.println("“Số này không nằm trong khoảng từ 15 đến 90");
        }

        // bài 6
        // a
        // CREATE DATABASE myfriends;

        // USE myfriends;

        // CREATE TABLE friend_list (
        // fl_id INT PRIMARY KEY,
        // flname VARCHAR(255)
        // );

        // INSERT INTO friend_list (fl_id, flname)
        // VALUES
        // (1, 'Bạn quen thời phổ thông'),
        // (2, 'Bạn quen thời đại học'),
        // (3, 'Bạn tâm giao'),
        // (4, 'Bạn chí cốt');

        // CREATE TABLE friends (
        // fid INT PRIMARY KEY,
        // fname VARCHAR(255),
        // preview_text TEXT,
        // date_create DATETIME,
        // fl_id INT,
        // FOREIGN KEY (fl_id) REFERENCES friend_list(fl_id)
        // );

        // INSERT INTO friends (fid, fname, preview_text, date_create, fl_id)
        // VALUES
        // (1, 'Nguyễn Văn Tèo', 'Bạn thân vẫn chia sẻ, trò chuyện trước ...',
        // '2015-01-14 07:17:42', 1),
        // (2, 'Trần Thị Diệu Thảo', 'Tôi và thằng bạn thân vẫn chia sẻ, trò chuyện...',
        // '2015-01-14 07:17:43', 1),
        // (3, 'Trần Việt Anh', 'Tôi và thằng bạn thân vẫn chia sẻ, trò chuyện trước',
        // NULL , 2),
        // (4,'Nguyễn Xuân Thành','Trò chuyện trước những tin vịt đã truyền qua lớp
        // khác.','2015-01-12 07:17:41',4),
        // (5,'Trần Thị Mỹ Hạ','Trước những tin vịt đã truyền qua lớp khác.',NULL ,3);

        // b
        // UPDATE friend_list
        // SET flname = 'Bạn tri kỷ'
        // WHERE fl_id = 4;

        // UPDATE friends
        // SET date_create = '2023-01-01 00:00:00'
        // WHERE date_create IS NULL;
    }
}
