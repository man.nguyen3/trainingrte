package baitap6;

import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        // bài 2
        giaiPTBacNhat();

        // bài 3
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập vào tên của bạn: ");
        String name = scanner.nextLine();
        System.out.println("Nhập vào ngày sinh của bạn (dd/mm/yyyy): ");
        String dob = scanner.nextLine();
        try {
            int year = Integer.parseInt(dob.substring(6));
            System.out.println(year);
            int age = 2023 - year;
            if (age < 13) {
                throw new AgeException("Tuổi của bạn phải trên 13");
            }
            System.out.println("Tên của bạn là " + name + ", tuổi của bạn là " + age);
        } catch (AgeException e) {
            System.out.println(e.getMessage());
            System.out.println("Vui lòng nhập lại thông tin của bạn");
        }

        // bài 4
        System.out.println("Nhập vào chuỗi ký tự: ");
        String str = scanner.nextLine();
        String[] words = str.split(" ");
        for (String word : words) {
            System.out.println(word);
        }
        System.out.println("Số từ trong chuỗi là " + words.length);
        StringBuilder sb = new StringBuilder(str);
        String chuoiMoi = sb.reverse().toString();
        System.out.println(chuoiMoi);

        //bài 5
        chuoiThayThe(str);

    }
    public static String chuoiThayThe(String str) {
        char[] chuoi = str.toCharArray();
        for (int i = 0; i < chuoi.length; i++) {
            if (chuoi[i] == 'e') {
                chuoi[i] = '9';
            } else if (chuoi[i] == 'g') {
                chuoi[i] = '3';
            }
        }
        return new String(chuoi);
    }

    /**
     * @param a
     * @param b
     * @return
     */
    public static void giaiPTBacNhat() {
        try {
            Scanner sc = new Scanner(System.in);
            System.out.println("Nhập số a ");
            Double a = sc.nextDouble();
            System.out.println("Nhập số b ");
            Double b = sc.nextDouble();
            if (a == 0) {
                if (b == 0) {
                    System.out.println("Phương trình vô nghiệm");
                } else {
                    System.out.println("Phương trình vô số nghiệm");
                }
            } else {
                System.out.println("Phương trình có nghiệm là : " + (-b / a));
            }
        } catch (Exception e) {
            System.out.println("Sai cú pháp");
        }
    }
}
