package baitap7;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class App {
    public static void main(String[] args) {

        // bài 1
        Integer number;
        ArrayList<Integer> list = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập vào số phần tử của ArrayList: ");
        Integer n = scanner.nextInt();
        for (int i = 0; i < n; i++) {
            System.out.print("Nhập phần tử thứ " + i + ": ");
            number = scanner.nextInt();
            list.add(number);
        }
        System.out.println("Tổng các phần tử của ArrayList: " + Sum(list));
        System.out.println("Tổng các phần tử của ArrayList: " + SumFirsttoLast(list));

        // bài 2
        System.out.print("Số lượng thí sinh : ");
        int m = scanner.nextInt();
        ArrayList<ThiSinh> danhSach = new ArrayList<>();
        for (int i = 0; i < m; i++) {
            ThiSinh thiSinh = new ThiSinh();
            thiSinh.NhapThongTin();
            danhSach.add(thiSinh);
        }
        System.out.println("Danh sach thí sinh trúng tuyển:");
        for (ThiSinh thiSinh : danhSach) {
            if (thiSinh.TinhTongDiem() >= 17) {
                thiSinh.InThongTin();
            }
        }
        System.out.println("Bảng xếp hạng");
        Collections.sort(danhSach, ThiSinh.cmpTongDiem);
    }

    public static Integer Sum(ArrayList<Integer> list) {
        int sum = 0;
        for (int num : list) {
            sum = sum + num;
        }
        return sum;
    }

    public static Integer SumFirsttoLast(ArrayList<Integer> list) {
        return list.get(0) + list.get((list.size() - 1));
    }
}
