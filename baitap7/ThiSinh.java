package baitap7;

import java.util.Comparator;
import java.util.Scanner;

public class ThiSinh {
    private String hoTen;
    private double diemToan;
    private double diemLy;
    private double diemHoa;

    public ThiSinh(String hoTen, double diemToan, double diemLy, double diemHoa) {
        this.hoTen = hoTen;
        this.diemToan = diemToan;
        this.diemLy = diemLy;
        this.diemHoa = diemHoa;
    }

    public ThiSinh() {}

    public String getHoTen() {
        return hoTen;
    }

    public void setHoTen(String hoTen) {
        this.hoTen = hoTen;
    }

    public double getDiemToan() {
        return diemToan;
    }

    public void setDiemToan(double diemToan) {
        this.diemToan = diemToan;
    }

    public double getDiemLy() {
        return diemLy;
    }

    public void setDiemLy(double diemLy) {
        this.diemLy = diemLy;
    }

    public double getDiemHoa() {
        return diemHoa;
    }

    public void setDiemHoa(double diemHoa) {
        this.diemHoa = diemHoa;
    }

    public void NhapThongTin() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Họ tên: ");
        this.hoTen = scanner.nextLine();
        System.out.print("Điểm toán: ");
        this.diemToan = scanner.nextDouble();
        System.out.print("Điểm lý: ");
        this.diemLy = scanner.nextDouble();
        System.out.print("Điểm hóa: ");
        this.diemHoa = scanner.nextDouble();
    }

    public double TinhTongDiem() {
        return diemToan + diemLy + diemHoa;
    }

    public void InThongTin() {
        System.out.println("Tên: " + hoTen);
        System.out.println("Điểm toán: " + diemToan);
        System.out.println("Điểm lý: " + diemLy);
        System.out.println("Điểm hóa: " + diemHoa);
        System.out.println("Tổng điểm: " + TinhTongDiem());
    }

    public static Comparator<ThiSinh> cmpTongDiem = new Comparator<ThiSinh>() {
        public int compare(ThiSinh ts1, ThiSinh ts2) {
            double tongDiem1 = ts1.TinhTongDiem();
            double tongDiem2 = ts2.TinhTongDiem();
            if (tongDiem1 < tongDiem2) {
                return 1;
            } else if (tongDiem1 > tongDiem2) {
                return -1;
            } else {
                return 0;
            }
        }
    };
}
