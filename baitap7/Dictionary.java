package baitap7;

import java.util.Scanner;

public class Dictionary {
    private static String[] englishWords;
    private static String[] vietnameseWords;
    private static String[] phoneticTranscriptions;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Nhập số lượng từ vựng: ");
        int numberOfWords = scanner.nextInt();
        scanner.nextLine();

        englishWords = new String[numberOfWords];
        vietnameseWords = new String[numberOfWords];
        phoneticTranscriptions = new String[numberOfWords];

        int choice;
        do {
            System.out.println("== TỪ ĐIỂN ANH VIỆT – BÀI TẬP VINAENTER EDU ==");
            System.out.println("--1. Nhập dữ liệu");
            System.out.println("--2. Xuất dữ liệu");
            System.out.println("--3. Dịch từ");
            System.out.println("--4. Thoát");
            System.out.print("Lựa chọn của bạn: ");
            choice = scanner.nextInt();
            scanner.nextLine();

            switch (choice) {
                case 1:
                    inputWords(scanner);
                    break;
                case 2:
                    outputWords();
                    break;
                case 3:
                    translateWord(scanner);
                    break;
                case 4:
                    System.out.println("Cảm ơn đã sử dụng từ điển!");
                    break;
                default:
                    System.out.println("Lựa chọn không hợp lệ!");
            }
        } while (choice != 4);
    }

    private static void inputWords(Scanner scanner) {
        for (int i = 0; i < englishWords.length; i++) {
            System.out.print("Nhập từ tiếng Anh thứ " + (i + 1) + ": ");
            englishWords[i] = scanner.nextLine();
            System.out.print("Nhập nghĩa tiếng Việt của từ " + englishWords[i] + ": ");
            vietnameseWords[i] = scanner.nextLine();
            System.out.print("Nhập phiên âm của từ " + englishWords[i] + ": ");
            phoneticTranscriptions[i] = scanner.nextLine();
        }
    }

    private static void outputWords() {
        for (int i = 0; i < englishWords.length; i++) {
            System.out.println(englishWords[i] + "\t" + vietnameseWords[i] + "\t" + phoneticTranscriptions[i]);
        }
    }

    private static void translateWord(Scanner scanner) {
        System.out.print("Nhập từ tiếng Anh cần dịch: ");
        String wordToTranslate = scanner.nextLine();
        boolean foundWord = false;
        for (int i = 0; i < englishWords.length; i++) {
            if (englishWords[i].equalsIgnoreCase(wordToTranslate)) {
                foundWord = true;
                System.out.println(vietnameseWords[i] + "\t" + phoneticTranscriptions[i]);
                break;
            }
        }
        if (!foundWord) {
            System.out.println("Không tìm thấy từ cần dịch!");
        }
    }
}
