package baitap3;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class HocVien {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<HocVien> list = new ArrayList<>();
        HocVien hocVien = new HocVien();
        
        System.out.print("Nhập mã học viên: ");
        String maHV = scanner.nextLine();
        hocVien.setMaHV(maHV);
        
        System.out.print("Nhập tên học viên: ");
        String tenHV = scanner.nextLine();
        hocVien.setTenHV(tenHV);
        
        System.out.print("Nhập lớp: ");
        String lop = scanner.nextLine();
        hocVien.setLop(lop);
        
        System.out.print("Nhập điểm Java: ");
        double diemJava = scanner.nextDouble();
        hocVien.setDiemJava(diemJava);
        
        System.out.print("Nhập điểm Oracle: ");
        double diemOracle = scanner.nextDouble();
        hocVien.setDiemOracle(diemOracle);
        
        System.out.print("Nhập điểm Project: ");
        double diemProject = scanner.nextDouble();
        hocVien.setDiemProject(diemProject);

        System.out.print("Thông tin của học viên là");
        xemThongTin();
        System.out.print("Điểm trung bình của học viên là");
        DTB(diemJava,diemOracle,diemProject);
        System.out.print("Điểm trung bình 3 học viên cao nhất là");
        MaxDTB(list);

    }

    

    public static void xemThongTin() {
        System.out.println("Mã học viên: " + maHV);
        System.out.println("Tên học viên: " + tenHV);
        System.out.println("Lớp: " + lop);
        System.out.println("Điểm Java: " + diemJava);
        System.out.println("Điểm Oracle: " + diemOracle);
        System.out.println("Điểm Project: " + diemProject);
    }
    private static double DTB(double diemJava, double diemOracle, double diemProject){
        double DTB = (diemJava + diemOracle + diemProject) / 3;
        return DTB;
    }
    public static double MaxDTB(List<HocVien> list) {
        double maxDTB = 0;
        for (HocVien hocVien : list) {
            double DTB_HocVien =  DTB(hocVien.getDiemJava(), hocVien.getDiemOracle(), hocVien.getDiemProject());
            if (DTB_HocVien > maxDTB) {
                maxDTB = DTB_HocVien;
            }
        }
        return maxDTB;
    }

    private static String maHV;
    private static String tenHV;
    private static String lop;
    private static double diemJava;
    private static double diemOracle;
    private static double diemProject;
    
    public String getMaHV() {
        return maHV;
    }
    
    public void setMaHV(String maHV) {
        this.maHV = maHV;
    }
    
    public String getTenHV() {
        return tenHV;
    }
    
    public void setTenHV(String tenHV) {
        this.tenHV = tenHV;
    }
    
    public String getLop() {
        return lop;
    }
    
    public void setLop(String lop) {
        this.lop = lop;
    }
    
    public double getDiemJava() {
        return diemJava;
    }
    
    public void setDiemJava(double diemJava) {
        this.diemJava = diemJava;
    }
    
    public double getDiemOracle() {
        return diemOracle;
    }
    
    public void setDiemOracle(double diemOracle) {
        this.diemOracle = diemOracle;
    }
    
    public double getDiemProject() {
        return diemProject;
    }
    
    public void setDiemProject(double diemProject) {
        this.diemProject = diemProject;
    }
}