package baitap3;

import java.util.Scanner;

public class exercise {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập số cho bài 1");
        int input1 = scanner.nextInt();
        dem(input1);
        System.out.println("Nhập số cho bài 2");
        int input2 = scanner.nextInt();
        int ran;
        ran = random();
        System.out.println("Số của chúng tôi là : " + ran);
        System.out.println(compare(input2, ran));
        System.out.println("Nhập số cho bài 3");
        int input3 = scanner.nextInt();
        System.out.println(convert(input3));

    }
    //bài 1
    public static void dem(int n) {
        int count3 = 0;
        int count5 = 0;
        for (int i = 0; i <= n; i++) {
            if (i % 5 == 0) {
                count5++;
            }
            if (i % 3 == 0) {
                count3++;
            }
        }
    System.out.println("Số lượng các số chia hết cho 3: " + count3);
    System.out.println("Số lượng các số chia hết cho 5: " + count5);
    }
    //bài 2
    public static int random() {
        int min = 8;
        int max = 18;
        return (int)(Math.random() * (max - min + 1) + min);
    }
    public static String compare(int a, int b) {
        if (a > b) {
            return "Số bạn nhập lớn hơn";
        } else if (a < b) {
            return "Số bạn nhập bé hơn";
        } else {
            return "Số bạn nhập bằng số của chúng tôi";
        }
    }
    public static String convert(int number) {
        if (number == 0) {
            return "không";
        }
    
        String[] donvi = {"", "một", "hai", "ba", "bốn", "năm", "sáu", "bảy", "tám", "chín", "mười",
                         "mười một", "mười hai", "mười ba", "mười bốn", "mười lắm", "mười sáu", "mười bảy", "mười tám", "mười chín"};
        String[] chuc = {"", "", "hai mươi", "ba muơi", "bốn mươi", "năm mươi", "sáu mươi", "bảy mươi", "tám mươi", "chín mươi"};
    
        String result = "";
        if (number < 20) {
            result += donvi[number];
        } else if (number < 100) {
            result += chuc[number / 10] + " " + donvi[number % 10];
        } else if (number < 1000) {
            result += donvi[number / 100] + " trăm " + convert(number % 100);
        } else {
            result += "ba số thôi fen";
        }
        return result;
    }
}
