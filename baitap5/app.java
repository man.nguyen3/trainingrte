package baitap5;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class app {
    public static void main(String[] args) {
        // bài 1
        int[] arr = new int[20];
        Random rand = new Random();
        for (int i = 0; i < arr.length; i++) {
            arr[i] = rand.nextInt(100);
        }
        System.out.println("20 arg with random: ");
        System.out.println(Arrays.toString(arr));
        System.out.println(sumArray(arr));
        System.out.println(sumEven(arr));
        System.out.println(averageOdd(arr));
        System.out.println(findMax(arr));

        // bài 2
        inputArray();
    }

    public static void arSoNguyen(){
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the size of the array: ");
        int size = scanner.nextInt();

        int[] numbers = new int[size];

        System.out.println("Enter the elements of the array:");
        for (int i = 0; i < size; i++) {
            int number;
            do {
                System.out.print("Enter element " + (i + 1) + ": ");
                number = scanner.nextInt();
            } while (number <= -100);
            numbers[i] = number;
        }

        System.out.println("The array you entered is:");
        for (int number : numbers) {
            System.out.print(number + " ");
        }
        int sum = 0;
        for (int number : numbers) {
            sum += number;
        }

        System.out.println("The sum of all elements in the array is: " + sum);
        int countPositive = 0;
        int sumPositive = 0;
        int countNegative = 0;
        int sumNegative = 0;

        for (int number : numbers) {
            if (number > 0) {
                countPositive++;
                sumPositive += number;
            } else if (number < 0) {
                countNegative++;
                sumNegative += number;
            }
        }

        System.out.println("Number of positive elements: " + countPositive);
        System.out.println("Sum of positive elements: " + sumPositive);
        System.out.println("Number of negative elements: " + countNegative);
        System.out.println("Sum of negative elements: " + sumNegative);
    }

    public static void inputArray() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the size of the array: ");
        int size = scanner.nextInt();
        int[] numbers = new int[size];
        System.out.println("Enter the elements of the array:");
        for (int i = 0; i < size; i++) {
            numbers[i] = scanner.nextInt();
        }

        int countPositive = 0;
        int sumPositive = 0;
        int countNegative = 0;
        int sumNegative = 0;

        int firstPositive = -1;
        int firstNegative = -1;

        for (int number : numbers) {
            if (number > 0) {
                countPositive++;
                sumPositive += number;
                firstPositive = number;
                break;
            } else if (number < 0) {
                countNegative++;
                sumNegative += number;
                firstNegative = number;
                break;
            }
        }

        double averagePositive = (double) sumPositive / countPositive;
        double averageNegative = (double) sumNegative / countNegative;

        System.out.println("Number of positive elements: " + countPositive);
        System.out.println("Sum of positive elements: " + sumPositive);
        System.out.println("Average of positive elements: " + averagePositive);
        System.out.println("Number of negative elements: " + countNegative);
        System.out.println("Sum of negative elements: " + sumNegative);
        System.out.println("Average of negative elements: " + averageNegative);
        System.out.println("First positive term: " + firstPositive);
        System.out.println("First negative term: " + firstNegative);
    }

    public static int sumArray(int[] arr) {
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            sum += arr[i];
        }
        return sum;
    }

    public static int sumEven(int[] arr) {
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 2 == 0) {
                sum += arr[i];
            }
        }
        return sum;
    }

    public static double averageOdd(int[] arr) {
        int sum = 0;
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 2 != 0) {
                sum += arr[i];
                count++;
            }
        }
        return (double) sum / count;
    }

    public static int findMax(int[] arr) {
        int max = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] > max) {
                max = arr[i];
            }
        }
        return max;
    }
}
