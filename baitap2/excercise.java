package baitap2;

import java.util.Random;
import java.util.Scanner;

public class excercise {
    public static void main(String[] args) {

        // bài 2
        Scanner input = new Scanner(System.in);
        Integer num = input.nextInt();
        Integer sum;
        sum = num * (num + 1) / 2;
        System.out.println(sum);
        // bài 3
        System.out.println("Nhập vào số năm");
        int year = input.nextInt();
        System.out.println("Nhập vào tháng");
        int thang = input.nextInt();
        if (thang == 1 || thang == 3 || thang == 5 || thang == 7 || thang == 8 || thang == 10 || thang == 12) {
            System.out.println("Tháng này có 31 ngày");
        } else if (thang == 4 || thang == 6 || thang == 9 || thang == 11) {
            System.out.println("Tháng này có 30 ngày");
        } else if (thang == 2) {
            if (((year % 400 == 0) && !(year % 100 == 0)) || (year % 400 == 0)) {
                System.out.println("Tháng này có 29 ngày");
            } else {
                System.out.println("Tháng này có 28 ngày");
            }
        } else {
            System.out.println("Tháng không hợp lệ");
        }
        // bài 4
        int min = Integer.MAX_VALUE;

        for (int i = 1; i <= 3; i++) {
            System.out.print("Nhập vào số thứ " + i + ": ");
            int num2 = input.nextInt();
            if (num2 < min) {
                min = num2;
            }
        }
        System.out.println("Số nhỏ nhất là: " + min);
        // bài 5
        Random random = new Random();
        int score = 0;

        while (true) {
            int randomNumber = random.nextInt(6); // Tạo ra một số ngẫu nhiên từ 0 đến 5
            System.out.print("Đoán một số từ 0 đến 5: ");
            int guess = input.nextInt();

            switch (guess) {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                    if (guess == randomNumber) {
                        System.out.println("Chúc mừng! Bạn đã đoán đúng.");
                        score += 2;
                    } else {
                        System.out.println("Rất tiếc! Bạn đã đoán sai. Số đúng là " + randomNumber);
                    }
                    break;
                default:
                    System.out.println("Số bạn nhập không hợp lệ");
                    break;
            }

            System.out.println("Điểm hiện tại của bạn là: " + score);

            System.out.print("Bạn có muốn chơi tiếp không? (Y/N): ");
            String playAgain = input.next();

            if (!playAgain.equalsIgnoreCase("Y")) {
                break;
            }
        }
        // bài 6
        System.out.print("Nhập vào chiều cao: ");
        int n = input.nextInt();

        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= n - i; j++) {
                System.out.print(" ");
            }
            for (int k = 1; k <= 2 * i - 1; k++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }
}