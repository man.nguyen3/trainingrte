package baitap4;

import java.util.Scanner;

public class SinhVien {

    private Integer MaSV;
    private String TenSV;

    public SinhVien(){

    }

    public SinhVien(Integer masv, String tensv){
        this.MaSV = masv;
        this.TenSV = tensv;
    }

    public String getTenSV() {
        return TenSV;
    }

    public void setTenSV(String tenSV) {
        this.TenSV = tenSV;
    }

    public Integer getMaSV() {
        return MaSV;
    }

    public void setMaSV(Integer masv2) {
        this.MaSV = masv2;
    }

    public void enter(Scanner sc){
        System.out.print("\tNhập Tên: ");
        TenSV = sc.nextLine();
        this.setTenSV(TenSV);
        System.out.print("\tNhập mã: ");
        MaSV = sc.nextInt();
        this.setMaSV(MaSV);
    }

    public void display() {
        System.out.println("\tTên: " + TenSV);
        System.out.println("\tMã: " + MaSV);
    }
}
