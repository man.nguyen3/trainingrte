package baitap4;

import java.util.Scanner;

public class HocSinh {
    public String hoTen;
    public String lop;
    public Double toan;
    public Double ly;
    public Double hoa;

    public HocSinh() {

    }

    public HocSinh(Double toan, Double ly, Double hoa) {
        this.hoa = hoa;
        this.toan = toan;
        this.ly = ly;
    }

    public String getLop() {
        return lop;
    }

    public void setLop(String lop) {
        this.lop = lop;
    }

    public String getHoten() {
        return hoTen;
    }

    public void setHoten(String hoTen) {
        this.hoTen = hoTen;
    }

    public Double getToan() {
        return toan;
    }

    public void setToan(Double toan) {
        this.toan = toan;
    }

    public Double getLy() {
        return ly;
    }

    public void setLy(Double ly) {
        this.ly = ly;
    }

    public Double getHoa() {
        return hoa;
    }

    public void setHoa(Double hoa) {
        this.hoa = hoa;
    }

    // Ham hien thi thong tin
    public void display() {
        System.out.println("\tTên: " + hoTen);
        System.out.println("\tLớp: " + lop);
        System.out.println("\tToán: " + toan);
        System.out.println("\tLý: " + ly);
        System.out.println("\tHóa: " + hoa);
    }

    // Ham nhap thong tin
    public void enter(Scanner sc) {
        System.out.print("\tNhập Tên: ");
        hoTen = sc.nextLine();
        System.out.print("\tNhập lớp: ");
        lop = sc.nextLine();
        System.out.print("\tNhập điểm toán: ");
        toan = sc.nextDouble();
        System.out.print("\tNhập điểm lý: ");
        ly = sc.nextDouble();
        System.out.print("\tNhập điểm hóa: ");
        hoa = sc.nextDouble();
    }
    //Ham tinh trung binh cong
    public Double TBC(Double a, Double b, Double c){
        this.toan = a;
        this.ly = b;
        this.hoa = c;
        return (a + b + c)/3;
    }
}
