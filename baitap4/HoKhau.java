package baitap4;

import java.util.Scanner;

public class HoKhau extends NhanKhau {
    private String diaChi;
    private String noiCongTac;

    public HoKhau(String hoTen, String ngaySinh, String gioiTinh, String diaChi, String noiCongTac) {
        super(hoTen, ngaySinh, gioiTinh);
        this.diaChi = diaChi;
        this.noiCongTac = noiCongTac;
    }

    @Override
    public void nhapThongTin() {
        super.nhapThongTin();
        Scanner sc = new Scanner(System.in);
        System.out.print("Nhap dia chi: ");
        diaChi = sc.nextLine();
        System.out.print("Nhap noi cong tac: ");
        noiCongTac = sc.nextLine();
    }

    @Override
    public void inThongTin() {
        super.inThongTin();
        System.out.println("Dia chi: " + diaChi);
        System.out.println("Noi cong tac: " + noiCongTac);
    }
}

