package baitap4;

import java.util.ArrayList;
import java.util.Scanner;

public class QuanLiDanSo {
    private ArrayList<NhanKhau> danhSachNhanKhau = new ArrayList<>();

    public void themNhanKhau() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Nhap ho ten: ");
        String hoTen = sc.nextLine();
        System.out.print("Nhap ngay sinh: ");
        String ngaySinh = sc.nextLine();
        System.out.print("Nhap gioi tinh: ");
        String gioiTinh = sc.nextLine();
        NhanKhau nhanKhau = new NhanKhau(hoTen, ngaySinh, gioiTinh);
        danhSachNhanKhau.add(nhanKhau);
    }

    public void themHoKhau() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Nhap ho ten chu ho: ");
        String hoTen = sc.nextLine();
        System.out.print("Nhap ngay sinh chu ho: ");
        String ngaySinh = sc.nextLine();
        System.out.print("Nhap gioi tinh chu ho: ");
        String gioiTinh = sc.nextLine();
        System.out.print("Nhap dia chi: ");
        String diaChi = sc.nextLine();
        System.out.print("Nhap noi cong tac: ");
        String noiCongTac = sc.nextLine();
        HoKhau hoKhau = new HoKhau(hoTen, ngaySinh, gioiTinh, diaChi, noiCongTac);
        danhSachNhanKhau.add(hoKhau);
    }

    public void inDanhSach() {
        for (int i = 0; i < danhSachNhanKhau.size(); i++) {
            System.out.println("Thong tin nguoi thu " + (i+1) + ":");
            danhSachNhanKhau.get(i).inThongTin();
            System.out.println();
        }
    }
}

