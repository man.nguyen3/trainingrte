package baitap4;

import java.util.Scanner;

public class SinhvienCNTT extends SinhVien{
    private Double dtoan;
    private Double dtin;

    public Double getdtoan() {
        return dtoan;
    }

    public void setdtoan(Double dtoan) {
        this.dtoan = dtoan;
    }

    public Double getdtin() {
        return dtin;
    }

    public void setdtin(Double dtin) {
        this.dtin = dtin;
    }

    public SinhvienCNTT(){

    }

    public SinhvienCNTT(Double dt, Double dti){
        this.dtin = dti;
        this.dtoan = dt;
    }
    
    public SinhvienCNTT(Double dt, Double dti,Integer masv, String tensv){
        setMaSV(masv);
        setTenSV(tensv);
        this.dtoan = dt;
        this.dtin = dti;
    }

    @Override
    public void enter(Scanner scan){
        super.enter(scan);
        System.out.print("\tNhập điểm toán: ");
        dtoan = scan.nextDouble();
        this.setdtoan(dtoan);
        System.out.print("\tNhập điểm tin: ");
        dtin = scan.nextDouble();
        this.setdtin(dtin);
    }

    @Override
    public void display(){
        super.display();
        System.out.println("\tĐiểm tin: " + dtin);
        System.out.println("\tĐiểm toán: " + dtoan);
    }
}
